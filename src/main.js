import { createApp } from 'vue'
import { createHead } from '@vueuse/head'
import App from './App.vue'
import './index.css'

const head = createHead()

createApp(App).use(head).mount('#app')
